import React, {useState} from "react";
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import wateringImg from "../assets/watering.png";
import { getStatusBarHeight } from 'react-native-status-bar-height';
import colors from "../styles/colors";
import fonts from "../styles/fonts"

import {Feather} from '@expo/vector-icons'

export function Welcome() {

  const [visible, setVisible] = useState(false)

  function handleVisibility() {
    setVisible(true)
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Gerencie {"\n"}
        suas plantas de{"\n"}
        forma fácils
      </Text>

      <Image style={styles.image} source={wateringImg} resizeMode="contain" />

      <Text style={styles.subtitle}>
        Não esqueça mais de regar suas plantas. Nós cuidamos de lembrar você
        sempre que precisar.
      </Text>

      <TouchableOpacity activeOpacity={0.7} style={styles.button}>
        <Feather name="chevron-right" style={styles.buttonIcon} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: getStatusBarHeight(),
    paddingHorizontal: 20
  },
  title: {
    fontSize: 28,
    fontWeight: "bold",
    textAlign: "center",
    color: colors.heading,
    marginTop: 38,
    fontFamily: fonts.heading,
    lineHeight: 34,
  },
  subtitle: {
    textAlign: "center",
    fontSize: 18,
    paddingHorizontal: 20,
    color: colors.heading,
    fontFamily: fonts.text,
    lineHeight: 25
  },
  image: {
    height: Dimensions.get("window").width * 0.9,
  },
  button: {
    backgroundColor: colors.green,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 16,
    marginBottom: 10,
    height: 56,
    width: 56,
  },

  buttonIcon: {
    color: colors.white,
    fontSize: 32,
  },
});